provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region = "us-east-1"
}

resource "aws_instance" "terraformvm" {
  ami = "ami-b374d5a5"
  instance_type = "t2.micro"
  key_name = "${aws_key_pair.deployer.key_name}"
}

resource "aws_eip" "ip" {
  instance = "${aws_instance.terraformvm.id}"
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAnvPD1qWiZFkhkNgd9fjuRdxaoZPsTxPb3CQllMjy5WVl1ulZBDiEHAYgfFFkGV2VN2N72toJ7f0ww8fYoTrwgarU0ysjSaSJjsQrUUKh35NwzcFbegBGMHbCTQYOmOpQ0SYN86homYyZsjGxuUWwrqV4aB6fUaqz13fOiZgx6z92t7csDIVc2EThiai39GHEoHcA/gsTFEoQBj9A9vgl9PEEMV+m2wYZu/meuhSrNkQtoxMelkg18Q61lyhRD71fg6SBe8RGNDOK+NytZyy+tgnOiL0JtpiOnQPlURMLmfousy+1giEsbwcXcLpGmlvWixj7eRgJn6oV+z8uxEeeew== rsa-key-20200120"
}

output "ip" {
  value = "${aws_eip.ip.public_ip}"
}